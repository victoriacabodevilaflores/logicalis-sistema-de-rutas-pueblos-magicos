/*

EQUIPO LOGICALIS
*/

/* Pueblos y sus correspondientes indices */
pueblo(1,'Orizaba').
pueblo(2,'Coscomatepec').
pueblo(3,'Xico').
pueblo(4,'Coatepec').
pueblo(5,'Papantla').
pueblo(6,'Zozocolco').


/* Grafo de distancias
 */
grafo(1,2,37).
grafo(1,3,141).
grafo(1,4,151).
grafo(1,5,407).
grafo(1,6,472).
grafo(2,3,105).
grafo(2,4,114).
grafo(2,5,370).
grafo(2,6,436).
grafo(3,4,9).
grafo(3,5,266).
grafo(3,6,331).
grafo(4,5,256).
grafo(4,6,321).
grafo(5,6,65).
grafo(6,5,65).
grafo(6,4,320).
grafo(6,3,331).
grafo(6,2,435).
grafo(6,1,472).
grafo(5,4,255).
grafo(5,3,266).
grafo(5,2,371).
grafo(5,1,407).
grafo(4,3,11).
grafo(4,2,114).
grafo(4,1,150).
grafo(3,2,105).
grafo(3,1,141).
grafo(2,1,36).



/* Rutas */
ruta(1,2,'N 1').
ruta(1,3,'N 2').
ruta(1,4,'N 3').
ruta(1,5,'N 4').
ruta(1,6,'N 5').
ruta(2,3,'N 6').
ruta(2,4,'N 7').
ruta(2,5,'N 8').
ruta(2,6,'N 9').
ruta(3,4,'N 1O').
ruta(3,5,'N 11').
ruta(3,6,'N 12').
ruta(4,5,'N 13').
ruta(4,6,'N 14').
ruta(5,6,'N 15').
ruta(6,1,'N 16').
ruta(6,2,'N 17').
ruta(6,3,'N 18').
ruta(6,4,'N 19').
ruta(6,5,'N 20').
ruta(5,4,'N 21').
ruta(5,3,'N 22').
ruta(5,2,'N 23').
ruta(5,1,'N 24').
ruta(4,3,'N 25').
ruta(4,2,'N 26').
ruta(4,1,'N 27').
ruta(3,2,'N 28').
ruta(3,1,'N 29').
ruta(2,1,'N 30').
/* Kilometros */
kilometrosViaje(Origen, Destino, Kms):-
     grafo(Origen, Destino, Kms).
/* Precio */
precio(1,2,250).
precio(1,3,920).
precio(1,4,870).
precio(1,5,1820).
precio(1,6,3300).
precio(2,3,740).
precio(2,4,800).
precio(2,5,2500).
precio(2,6,3050).
precio(3,4,70).
precio(3,5,1800).
precio(3,6,2300).
precio(4,5,1700).
precio(4,6,2250).
precio(5,6,460).
precio(6,5,460).
precio(6,4,2250).
precio(6,3,2300).
precio(6,2,3050).
precio(6,1,3300).
precio(5,4,1700).
precio(5,3,1800).
precio(5,2,2500).
precio(5,1,1820).
precio(4,3,80).
precio(4,2,800).
precio(4,1,870).
precio(3,2,740).
precio(3,1,920).
precio(2,1,250).